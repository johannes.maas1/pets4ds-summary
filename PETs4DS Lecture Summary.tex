\documentclass[nobib]{tufte-handout}

\usepackage[utf8]{inputenc}
\usepackage{csquotes}
\usepackage{bm}
\usepackage{ntheorem}

\usepackage{listings}
\lstset{
  basicstyle=\ttfamily
}

\usepackage[
  backend=biber,
  style=authoryear,
  maxbibnames=5,
  ]{biblatex}

\addbibresource{literature.bib}

\newcommand{\sidecite}[2][]{\sidenote{\autocite[#1]{#2}}}
\newcommand{\margincite}[2][]{\marginnote{\autocite[#1]{#2}}}

\DeclareCiteCommand{\shortfullcite}
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   \printnames[-1]{labelname}%
   \setunit{\labelnamepunct}
   \printfield[citetitle]{title}%
   \newunit
   \usebibmacro{date}
   \newunit
  }
  {\addsemicolon\space}
  {\usebibmacro{postnote}}

\renewbibmacro*{date}{%
  \printfield{year}
  \iffieldundef{origyear}{%
  }{%
    \setunit*{\addspace}%
    \printtext[brackets]{\printorigdate}%
  }%
}


\newcommand{\link}[2]{\href{#2}{#1}\sidenote{\url{#2}}}

%\geometry{showframe} % display margins for debugging page layout

\usepackage{graphicx} % allow embedded images
  \setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
  \graphicspath{{graphics/}} % set of paths to search for images
\usepackage{amsmath}  % extended mathematics
\usepackage{booktabs} % book-quality tables
\usepackage{units}    % non-stacked fractions and better unit spacing
\usepackage{multicol} % multiple column layout facilities
\usepackage{lipsum}   % filler text
\usepackage{fancyvrb} % extended verbatim environments
  \fvset{fontsize=\normalsize}% default font size for fancy-verbatim environments

% Standardize command font styles and environments
\newcommand{\doccmd}[1]{\texttt{\textbackslash#1}}% command name -- adds backslash automatically
\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
\newcommand{\docenv}[1]{\textsf{#1}}% environment name
\newcommand{\docpkg}[1]{\texttt{#1}}% package name
\newcommand{\doccls}[1]{\texttt{#1}}% document class name
\newcommand{\docclsopt}[1]{\texttt{#1}}% document class option name
\newenvironment{docspec}{\begin{quote}\noindent}{\end{quote}}% command specification environment


% Matrix, abbreviated mat.
\newcommand{\mat}[1]{\ensuremath{\bm{\mathrm{#1}}}}

\newcommand{\name}[1]{\ensuremath{\mathrm{#1}}}

\theoremstyle{break}
\theorembodyfont{}
\theoremheaderfont{\normalfont\itshape}
\newtheorem{env:definition}{Definition}

% \definition{ Name }{ Description }{ Formal definition }
\newcommand{\definition}[3]{
  \noindent{\textbf{#1}}\\
  #2
  \begin{env:definition}
  #3
  \end{env:definition}
}

\newcommand{\secref}[1]{section~\emph{\nameref{#1}}}

\begin{document}

\title{Privacy Enhancing Technologies}
\author{Johannes Maas}
%\date{28 March 2010} % without \date command, current date is supplied

\maketitle% this prints the handout title, author, and date

\begin{abstract}
  A summary of the lecture \link{\emph{Privacy Enhancing Technologies for Data Science}}{http://dbis.rwth-aachen.de/cms/teaching/WS1718/PETs4DS} held during the winter semester 2018/19 by \link{Benjamin Heitmann, Ph.D.}{http://dbis.rwth-aachen.de/cms/staff/heitmann} at RWTH Aachen University.
\end{abstract}

%\printclassoptions

\section{Definitions}
Privacy differs from security. Security is about preventing people who should not have access to data from getting them. Privacy is about how a rightful data holder treats that data.

\section{Threat model}
A threat model is a set of aspects under which to analyze a system for potential threats. An example is the security threat model STRIDE. The first two aspects therein are spoofing and tampering (the S and T respectively). The idea is to structure the analysis of the system. First it is evaluated for potential ways that a user's identity can be spoofed. Then it should be analyzed for ways that the data could be tampered with, etc.

Such a threat model also exists for privacy. It is called LINDDUN\sidecite{noauthor_linddun_2014} and contains the aspects
\begin{description}
  \item[Linkability]
    Identifying that two items are linked. An internal link is enough, there does not need to be a link to the real world. Background knowledge of the attacker has to be assumed.

    A system for rating the security of websites is vulnerable to linking, if it stores identifiers of the people who visited a URL. Multiple URL visits can be linked together to form a browsing history of a person, even if that person is anonymous.
  \item[Identifiability]
    Linking an item to a real-world identity.

    A system that stores the names and addresses of people is trivially vulnerable to identification.
  \item[Non-repudiation]
    Proving that someone has done a certain action.\sidenote{A system with repudiation allows for plausible deniablity.}

    A voting system, where each vote bears the name of the voter is a system that is vulnerable to non-repudiation.
  \item[Detectability]
    Proving the existence of an item.

    Password reset systems that inform the user that the entered email address is not registered make it possible to detect whether there is an account under a specific email address in that system.
  \item[Disclosure of information]
    Accessing data without authorization.

    Unencrypted emails may be read by the internet service provider, thus being at risk of information disclosure.
  \item[Unawareness]
    Letting the user in the dark about certain consequences. Either purposefully or accidentally. 

    Sharing user data with an advertisement network, without informing the user, can lead to an unawareness risk.
  \item[Non-compliance]
    Not following rules. The rules can be external, e. g. laws, or internal, e. g. a EULA.

    Stating in the EULA that no data is shared with third parties, but sharing it anyway, is a trivial example of non-compliance.
\end{description}

The last two threats are often grouped as soft privacy, whereas the others are grouped as hard privacy. The difference is that hard privacy gives quantitative guarantees, whereas soft privacy is more about social duties.

\subsection{Data flow diagram}
A data flow diagram (DFD) models what components touch what data. It is composed of
\begin{description}
  \item[Processes] The system components operating on data.
  \item[Data stores] Places where data is persisted.
  \item[External entities] Actors that interacts with the system.  
  \item[Data flow] Directed connections between two items in the DFD, indicating that they exchange data.
\end{description}

The DFD can be analyzed for LINDDUN threats. However, not all threats apply to all items. Processes and data stores are vulnerable to all but unawareness. External entities are only vulnerable to linkability, identifiability, and unawareness.

\section{Anonymization of tabular data}
Tabular data can be classified into
\begin{description}
  \item[Identifiers] Allow to directly link an entry to a real-world identity.
  \item[Quasi-identifiers] Provide some information about the entry's link to the real world. Typically not enough for identification on their own, but can lead to identification when enough quasi-identifiers are known or when combined with background knowledge. 
  \item[Sensitive attributes] The information that should not be linked to the real-world identity, but that is relevant to the analysis.
\end{description}

To achieve anonymity, identifiers can never be shared. The value of the dataset, though, lies in the relationships between the quasi-identifiers and the sensitive attributes. For example, a medical data base could contain valuable insight into what regions (post code as quasi-identifiers) have a high risk for certain illnesses (illnesses as sensitive attributes). Therefore, we cannot simply remove quasi-identifiers. Instead, we need to find a tradeoff between protecting individual entries and protecting the relationship to the sensitive data.

\subsection{$k$-anonymity}
The simplest attack on a database where identifiers are removed is to exploit the uniqueness of quasi-identifiers to link the entry to the real world. For example, if there is only one person with age 23 in a medical database, and we are interested in knowing the illness of a 23-year-old person we know is in that database, than even without the identifier, we can discover their illness.

To protect against this, $k$-anonymity requires that at least $k$ entries in the dataset are indistinguishable regarding their quasi-identifiers. This loses some accuracy in the correlation.

An algorithm to achieve $k$-anonymity modifies the dataset iteratively. It starts at some attribute, checks whether it fulfills $k$-anonymity by counting the number of entries that share the same attribute value. If some entries are too unique, i. e. there are less than $k$ entries with the same value, the entries need to be grouped together with others to achieve $k$-anonymity.

Grouping entries together requires a concept hierarchy, which depends on the attribute. For example, ages can be grouped into buckets spanning 10 years, then buckets spanning 20 years, and finally be suppressed by grouping them into a single bucket, which makes the attribute useless. Post codes can be generalized by censoring the last digit, then the second digit as well, and so on.

The information loss of this algorithm is the relative number of levels that the value moved up in the concept hierarchy, namely the difference in level divided by the total levels. The loss for an entry is the sum of all individual attributes' losses. \label{concept-hierarchy-level}Formally, for a concept hierarchy $C$ with a total of $l_n$ levels, and function $l(v)$ giving the level of $v$ in $C$, the information loss of the anonymized value $v_a$ is\sidenote{The lecture defines the information loss between the original value $v$ and $v_a$ as $\frac{|l(v)-l(v_a)|}{l_n}$. Since $l(v)$ is always $0$, this formula is a simplification.}
\begin{equation*}
  \frac{l(v_a)}{l_n}
\end{equation*}

\subsection{$l$-diversity}\margincite{machanavajjhala_l-diversity:_2007}
A homogeneity attack on $k$-anonymity allows an attacker to learn valuable information about a target. Consider a similar case to before where we want to learn the illness of a 23-year-old. If the dataset is $10$-anonymized, but all entries with age \lstinline{20-30} have the same illness, then we will have successfully learned what we wanted to know.

$l$-diversity mitigates the homogeneity attack on $k$-anonymity by requiring that in each equivalence class\sidenote{The equivalence classes are the groups of entries with the same quasi-identifiers.} there are at least $l$ \enquote{well-represented} sensitive attributes. There are different interpretations of how attributes are \enquote{well-represented}.

The simplest approach is to require $l$ distinct sensitive attributes. However, this approach may lead to a highly skewed distribution in an equivalence class. For example, a class with one count of mental illness and ninety-nine counts of physical illness would satisfy $2$-diversity. If usually there is an equal chance of having a mental or a physical disease, an attacker would know that someone inside that equivalence class has an extremely high chance of having a physical illness.

This flaw can be mitigated by employing entropy $l$-diversity. With more uniform distributions of values inside an equivalence class, entropy goes up. Entropy $l$-diversity requires that the average entropy in each equivalence class is at least $\log(l)$: Let $p_{eq}(s)$ be the probability of entry $s\in S$ in equivalence class $eq$. The entropy of $eq$ is defined as 
\begin{equation*}
  -\sum_{s\in S} p_{eq}(s) \cdot \log(p_{eq}(s))
\end{equation*}

\subsection{$t$-closeness}\margincite{li_t-closeness:_2007}
Attacks on $l$-diversity exploit that it does not account for the meaning of attributes. For example, in a database containing test results for HIV, a negative result is not considered sensitive. But $l$-diversity would not allow an equivalence class with only negative results. Similarly, there might be an equivalence class with different salary brackets that satisfies $l$-diversity. But if all the occurring salaries are relatively low,\sidenote{Compared to the population's average.} an attacker has learned valuable information.
Another attack concerns the difference of the distribution inside an equivalence class to the population's distribution. If, for example, we have an equivalence class where half HIV test results are positive, half negative, then an attacker would learn that people inside this class have an elevated chance of a positive result.

The idea of $t$-closeness is to measure the distance of the distribution inside an equivalence class to the population's distribution. There are multiple ways of measuring that distance.

Variational distance calculates the mean point-wise difference in probability. Given two distributions $P=(p_1,\ldots,p_m)$ and $Q=(q_1,\ldots,q_m)$,\sidenote{$p_i$ is the probability of value $v_i$ occuring in equivalence class $eq_P$. Analogously for $q_i$.} the variational distance is defined as
\begin{equation*}
  \sum_{i=0}^m \frac{1}{2} |p_i-q_i|
\end{equation*}

The disadvantage of this measure is its inability to capture the relationship between points, i. e. the semantics. Consider an equiprobable distribution. An equivalence class with three entries in the lower end of the domain will have the same variational distance as three entries that are spread out over the domain, even though the latter is intuitively closer to the equiprobable population.

The earth mover's distance captures this intuition. Metaphorically, it calculates the work of moving the probability mass hills of one distribution into the holes of the other. If the hills are already spread out, there is less moving to do to reach every hole.

In its essence, the earth mover's distance is an optimization problem. But there exist closed-form equations for numerical and categorical attributes. The closed form for numerical attributes is
\begin{equation*}
  \frac{1}{m-1}\sum_{i=1}^m \left|\sum_{j=1}^i p_j-q_j\right|
\end{equation*}
The closed form for categorical attributes is defined for the set of non-leaf nodes $NL$ as
\begin{equation*}
  \sum_{n\in NL} cost(n)
\end{equation*}
where with $Child(n)$ being the set of all direct children of $n$, and information loss of $n$, denoted $I(n)$, as defined in \secref{concept-hierarchy-level},
\begin{align*}
  cost(n)&:=I(n) \min\{pos\_extra(n), neg\_extra(n)\}\\
  pos\_extra(n)&:=\sum_{c\in Child(n) \land extra(c)>0} |extra(c)|\\
  neg\_extra(n)&:=\sum_{c\in Child(n) \land extra(c)<0} |extra(c)|\\
  extra(n)&:=\begin{cases}
    p_i-q_i & \text{if $n$ is a leaf,}\\
    \sum_{c\in Child(n)} extra(c) & \text{otherwise.}
  \end{cases} 
\end{align*}

\section{Anonymization of graph data}
A graph's neighborhood is its most valuable characteristic. Even without node labels, passive attacks can deduce a lot from just the neighborhood and some background information. Active attacks try to insert subgraphs with unique neighborhoods in order to identify them in the anonymized data.

\subsection{$k$-degree anonymity}\margincite{liu_towards_2008}
$k$-degree anonymity requires at least $k$ nodes to share the same degree of edges.
Since there are potentially multiple distributions of degrees that satisfy $k$-degree anonymity, the one with the smallest loss of information should be determined. Then a new, or modified, graph can be constructed.

Degree distributions can be encoded in a degree sequence, defined as a vector $d$ such that $d_i$ is the degree of node $v_i$. For convenience, the vector is ordered by decreasing degree. The cost of putting the nodes $i$ to $j$ into the same class is
\begin{equation*}
  I(d[i,j]):=\sum_{l=i}^j d_j - d_l
\end{equation*}
The first $2k-1$ nodes must be in the same group.\sidenote{It is impossible to make two groups of size at least $k$ out of less than $2k$ nodes.} Therefore, there is a case distinction in the total cost formula
\begin{equation*}
  D(d[1,i]):=\begin{cases}
    I(d[1,i]) & \text{for $i<2k$,}\\
    \min\left\{\begin{aligned}
      &\min_{k\leq t\leq i-k}\{D(d[1,t])+I(d[t+1,i])\},\\
      &I(d[1,i])
    \end{aligned}\right\} & \text{for $i\geq 2k$.}
  \end{cases}
\end{equation*}
The values for $D$ can be cached as a cost matrix.

The construction of the graph requires a realizable degree sequence. Some sequences have too many edges in the high-degree nodes, that cannot be \enquote{absorbed} by the remaining nodes. A degree sequence $d$ with $n:=|d|$ is realizable iff all $l\in[1,n-1]_\mathbb{N}$ satisfy
\begin{equation*}
  \sum_{i=1}^l d_i\leq l(l-1)+\sum_{i=l+1}^n \min\{l,d_i\}
\end{equation*}
Using such a realizable degree sequence, either a completely new graph can be constructed that preserves only the degree information, or the original graph can be modified. To modify the graph, wherever the original and anonymized degree sequences differ, new edges must be added.

\subsection{$k$-neighborhood anonymity}\margincite{zhou_preserving_2008}
Not only the degree of a node is important, but also its neighbors' connections. $k$-neighborhood anonymity requires that there are at least $k$ nodes with the same neighborhood.

To achieve $k$-neighborhood anonymity, the neighborhoods of all nodes need to be compared for isomorphism. To make these comparisons more efficient, a neighborhood component coding is employed. Whenever a neighborhood is too unique, it is merged with the most similar neighborhoods to form a new equivalence class that satisfies $k$-neighborhood anonymity.

If the nodes are labelled, there needs to be a concept hierarchy that specifies what labels can be generalized to what new label, in case that two neighborhoods are structurally similar, but have different labels.

The anonymization cost is composed of the loss due to label generalization, edge addition, and node addition. Each of these parts can be tuned.

\subsection{$k$-sized grouping}\margincite{hay_resisting_2010}
Graphs can also be generalized in a way that keeps some broader connection information while removing the possibility of identifying individual nodes. A $k$-sized grouping is a partitioning of the graph, such that each group contains at least $k$ nodes. Each group is annotated with the number of original nodes it contains, and the edges between groups tell how many original edges run between the groups.

This new graph represents many possible original graphs, therefore making it impossible to deduce information about individual nodes. However, some amount of the vital neighborhood information is retained.

\section{Secure multi-party computation}\margincite{cramer_secure_2015} 
Computations can be broken down to circuits with few operations. One variant are arithmetic circuits, where operations include addition, multiplication, and multiplication with a constant. Another option are Boolean circuits, using logical and, or, as well as negation. A Boolean circuit can be encoded using truth tables.

\label{adversaries}We distinguish honest-but-curious attackers from malicious ones. An honest-but-curious adversary follows the rules of the protocol, but may collude and learn more information than one party should know. A malicious attacker may instead actively sabotage the protocol. Protecting against malicious attackers is difficult, therefore many protocols only guarantee safety against honest-but-curious adversaries.

\subsection{Yao's garbled circuit}
This algorithm encrypts Boolean circuits and requires symmetric encryption with error detection, and oblivious transfer.\sidenote{Oblivous transfer lets a chooser pick one of two alternatives from a provider. The provider does not know the choice, and the chooser can only learn about one alternative. The details exceed this lecture's scope.}

Alice encrypts the entire circuit by first representing each input and output with a randomly chosen encryption key. Then she transforms each truth table's rows by encrypting the key for the row's result with the keys corresponding to both inputs. For example, if the truth table row is $A=0,B=1\rightarrow 1$, the corresponding keys are $k_0^A$ (for $A=0$), $k_1^B$ (for $B=1$), and $k_1^r$ (for the result $1$). The transformed row is $E(k_0^A,E(k_1^B,k_1^r))$,\sidenote{Let $E(k,v)$ be the encryption of value $v$ using key $k$.} i. e. the double encryption of the result key using both input keys. In the end, Alice garbles the rows' order, to prevent Bob from knowing what encryption corresponds to what value.

Alice sends Bob the key that corresponds to her input, e. g. $k_0^A$. Bob then, using oblivious transfer, chooses Alice's key that corresponds to his input, e. g. $k_0^B$. Alice does not know which one he chose, and thus does not know his input, but Bob only has one key, and cannot decrypt more than one row of the garbled truth table. Then, Bob finds the entry in the truth table that can be decrypted with the available encrypted input. Following the notation above, the only entry he can decrypt is $E(k_0^A,E(k_0^B,k_0^r))$,\sidenote{All other entries use different keys, and thus result in an error when decryption is attempted.} therefore Bob now has decrypted the key $k_0^r$.

This key, $k_0^r$, is possibly the input to the next gate in the circuit. Each gate can be decrypted by Bob in the manner described above, yielding, in the end, an encryption of the final result, e. g. $E(k, 1)$. Bob does not know what value this cipher represents. He sends the result to Alice, and she send him the decryption key, $k$, such that Bob can decrypt the result for himself. Finally, both have learned the computation's result.

\subsection{Circuit evaluation with passive security (CEPS)}
CEPS is a protocol that allows secure multi-party computation. An essential technique is secret sharing, which is based on Lagrange interpolation.

For secret sharing, a polynomial can be secretly constructed that passes through the desired value at $x=0$. Then, supporting points of this polynomial are shared with the other parties. To recover the secret polynomial from the shares, Lagrange interpolation can be used.\sidenote{An explanation by example of secret sharing with Lagrange interpolation is at \url{https://youtu.be/kkMps3X_tEE}.} The Lagrange polynomial for party $i$ with supporting points $x_1,\ldots,x_n$ is defined as
\begin{equation*}
  l_i(x):=\prod_{j\in[0,n]_\mathbb{N}\land j\neq i}\frac{x-x_j}{x_i-x_j}
\end{equation*}
This allows the reconstruction of the original polynomial, $p$, using the shares $y_1,\ldots,y_n$:
\begin{equation*}
  p(x)=\sum_{i=1}^n l_i(x)\cdot y_i
\end{equation*}

 The secret sharing technique defined above already supports share-wise addition and multiplication by a constant homomorphically. Multiplication, though, has the caveat that it increases the degree of the polynomial, therefore risking that the result is not recoverable anymore.\sidenote{Multiplication of two polynomials of degree $d$ results in a polynomial of degree $2d$, which requires $2d+1$ supporting points for reconstruction.} To work around this limitation, the result of each share-wise multiplication is shared and reconstructed in a sub-protocol. After this, each party holds a share of the product, but with the degree reset. For example, if each party holds shares $y_i^1$ and $y_i^2$ that should be multiplied, then each party computes $y_i^1\cdot y_i^2$. Because the degree of their shares is now too high, they reconstruct the product by secretly sharing their local results. Now each party $i$ has received new shares $y_i^j$ from every other party $j$ that together still result in the product $y^1\cdot y^2$. Therefore, each party reconstructs its new supporting point $p(x_i)=\sum_{j=1}^n l_j(x)\cdot y_i^j$.

\subsection{Homomorphic encryption}
RSA is multiplicatively homomorphic, meaning that multiplications on the cypher can be correctly decoded. It lacks additive homomorphism. RSA deduces the keys from two prime numbers $p$ and $q$ by determining $N:=p\cdot q$, which serves as the modulus for all operations, and $\Phi(N):=(p-1)\cdot(q-1)$. The public key is some $e\in[2,\Phi(N)-1]_\mathbb{N}$ that is coprime to $\Phi(N)$,\sidenote{The greatest common denominator of two coprime numbers is $1$.} the secret key is the multiplicative inverse of $e$, i. e. $d\cdot e=1\mod \Phi(N)$.\sidenote{The multiplicative inverse can be determined by the extended Euclidian algorithm.} The encrypted cipher $c$ of plaintext $m$ is $c=m^e\mod N$, while decryption is $m=c^d\mod N$.

The Paillier cryptosystem is additively homomorphic. Additionally, it can encrypt the same plaintext with different ciphers, by adding a random value that is automatically removed during decryption.\sidenote{The feature of randomizing the cipher is useful for voting systems, because there are only few different plaintext values that get repeated a lot.} It uses two prime numbers $p$ and $q$, $n:=p\cdot q$, $\lambda:=\operatorname{lcm}(p-1,q-1)$, a special generator $g\in\mathbb{Z}_{n^2}^*$ with associated $\mu$. The public key is $(n,g)$, the private key is $(\lambda, \mu)$. A plaintext $m$ can be encrypted, with a random number $r\in \mathbb{Z}_n^*$, to cipher $c$ by $g^m r^n\mod n^2$, while decryption is $L(c^\lambda\mod n^2)\mu\mod n$ with $L(x):=\frac{x-1}{n}$.

Multiplication requires one of the factors to be plain. However, splitting the multiplication step between two parties can achieve some privacy. For this, one party gets the encryption key, the other the decryption key. The trick is that one party only sees encrypted data, without being able to decrypt it, and the other party sees unencrypted, but randomly masked data, that only the first party can unmask. The encrypting party masks the values by adding random numbers. Then the other party decrypts that masked data, does the multiplication, and sends back the encrypted result. The first party can then unmask the result without lifting the encyption.

\subsection{SPDZ}\margincite{safavi-naini_multiparty_2012}
In contrast to CEPS, the SPDZ protocol can detect manipulations of intermediary results, thereby offering active security and protecting against malicious adversaries as defined in \secref{adversaries}.

The protocol is split into an offline and an online phase. The offline phase is used for precomputation of message authentication codes (MACs), multiplication triplets, and additive shares of random numbers. MACs are used to check the results, making them the mechanism guaranteeing active security. Since the online phase uses an additively homomorphic scheme, multiplication triplets\sidenote{Detecting manipulation of multiplication triplets can be achieved by sacrificing one triplet to verify another.} will be used to enable multiplicative capabilities. The secretly shared random numbers are used for input sharing.\sidenote{The details of the offline phase exceed the lecture's scope.}

There is a single MAC key $\alpha$, that is not known to any party, but additively shared. The MAC value for input $a$ is $\gamma(a):=\alpha\cdot a$. For each secretly shared value $a$, the party $i$ will hold additive shares $a_i$ and $\gamma(a)_i$.

For a party to share an input, a shared random number is subtracted from the party's input. It then broadcasts that masked value, such that each party can compute an unmasked share.\sidenote{The MAC values for the input are calculated based on the precomputed MAC values associated with that random number.} For example, for party $i$ to share $a$, all shares of the random number $r$ are opened to $i$, such that $i$ knows the actual $r$. It then broadcasts $a-r$ and each party $j$ computes $a_i+r_i$, unmasking its share.

Addition and multiplication by a constant can be computed share-wise on both the inputs and their MAC values. For example, to compute $a+b$, party $i$ computes $a_i+b_i$ and $\gamma(a)_i+\gamma(b)_i$.

Multiplication uses the Beaver method\sidecite{feigenbaum_efficient_1992} that requires a multiplication triplet. Masked values of the factor to be multiplied are computed using the triplet, and then opened among all parties. Each party can then compute its share of the multiplication result. The MAC value is derived from the triplet. For example, to multiply $a\cdot b$, a triplet $x\cdot y=z$ is needed. The masked factors are $\epsilon:=a-x$ and $\delta:=b-y$. Party $i$ computes $z_i+y_i\cdot \epsilon+x_i\cdot \delta(+\epsilon\cdot\delta)$. Note that each party uses the actual, opened values of $\epsilon$ and $\delta$, not shares of them. The final summand, $\epsilon\cdot\delta$, needs to be added by only a single party. The MAC value party $i$ computes is $\gamma(z)_i+\epsilon\gamma(y)+\delta\gamma(x)+\alpha_i\epsilon\delta$.

To check the final result, it is opened to all parties. Each party can now check the result, by analyzing all opened values. For example, assume values $\Omega:=\{\epsilon,\delta,r\}$ have been opened.\sidenote{$r$ is supposed to be the result, the others are intemediarily opened values from a multiplication.} Then each of the $n$ parties can compute for every $i\in[1,n]_\mathbb{N}$ the codes
\begin{equation*}
  \sigma_i:=\left(\sum_{v\in\Sigma} \gamma(v)_i\right)-\alpha_i\left(\sum_{v\in\Sigma} v\right)
\end{equation*}
The result is correct, if $\sum_{i=1}^n \sigma_i=0$.

The expensive offline phase is improved by the successor protocols MASCOT and SPDZ 2.0. Instead of full homomorphic encryption for generating the multiplication triplets, MASCOT uses oblivious transfer. SPDZ 2.0 has a few different optimizations such as precomputations for other operations than just multiplication.

\section{Record linkage}
Linking entries in two databases together can create valuable new information, but also exposes the entries to more risk. Either a third party can act as an intermediary, or the two parties can use involved methods for protecting the data.

An exact match hash encoding simply hashes the entries in order to compare them. Equal entries will result in equal hashes, enabling the identification of matches. However, there can be frequency attacks, where the distribution of hashes resembles the distribution characterizing certain data. Additionally, practical problems arise, since oftentimes entries that should be linked do not match exactly.

To mitigate the problem of non-exact matches, Bloom filters can be used. They are bit vectors alongside a secret set of hash functions. Each entry is split into smaller pieces, and each piece is passed to every hash function, which will return a position in the vector. Each such position is set to $1$. Since two equal pieces will effectively set the same bits in the vector, comparing two Bloom filters' equal bits gives clues to their elements' similarity.\sidenote{Note that Bloom filters might lead to false positives, because two different elements can be mapped to the same positions. However, false negatives are not possible.}

\section{Differential privacy}\margincite{dwork_algorithmic_2013}
The guarantee differential privacy gives is that the dataset would appear the same, if individual entries would not exist. The main mechanism is to add noise that will cancel out itself when evaluated.

A function satisfies $\epsilon$-differential privacy iff for all datasets differing in one entry the result does not change more than a factor $e^\epsilon$. That is, a function $K$ should, for all datasets $d_1$ and $d_2$ that differ in only one element, and all $S\subseteq\operatorname{im}(K)$,\sidenote{Subsets of $K$'s image.} satisfy
\begin{equation*}
  Pr(K(d_1)\in S)\leq e^\epsilon Pr(K(d_2)\in S)
\end{equation*}

$\epsilon$-differential privacy can be achieved by adding Laplacian noise. The parameters of the noise depend only on the sensitivity of the function\sidenote{The sensitivity of a function is the biggest difference that changing a single element can make.} and $\epsilon$, and can therefore be precomputed for each function. This can be used to protect queries, since they can be modeled as functions. For example, a query $f$, with Laplacian noise denoted $Lap(\mu, b)$\sidenote{$Lap(\mu, b):=\frac{1}{2b} \exp \left( -\frac{|x-\mu|}{b} \right)$}, and the sensitivity of $f$ denoted $\Delta f$, can be transformed to a function satisfying $\epsilon$-differential privacy:\sidecite[Definition 3.3]{dwork_algorithmic_2013}
\begin{equation*}
  K(x):=f(x)+Lap(0, \frac{\Delta f}{\epsilon})
\end{equation*}

\subsection{Local differential privacy}\margincite{differential_privacy_team_apple_learning_2017}
Differential privacy is usually employed on a central server, that takes in the raw data. An improvement is $\epsilon$-local differential privacy, where there is a client-side preprocessing step that anonymizes the data, and the server only does post-processing on that anonymized data.

The count mean sketch algorithm (CMS) that privately estimates mean counts of elements requires client and server to share a set of hash functions, $H=\{h_1,h_2,\ldots,h_k\}$. To anonymize a record $r$, the client starts with a bit vector $v$ that is filled with $-1$. It then chooses a hash function from $h_i\in H$ randomly to map the record to a position in that vector, which it sets to $1$, i. e. $v_{h_i(r)}:=1$. Then it anonymizes the vector by independently flipping each bit with probability $\frac{1}{exp(\frac{\epsilon}{2})+1}$. This vector, $v^a$, together with the chosen hash function, $h_i$, is sent to the server.
The server will aggregate these submissions in a sketch matrix $M$. For each hash function $h_i$, there is a row $i$ in the matrix. A submission $(h_i, v^a)$ is aggregated by adding its vector $v_a$ to the $i$th row in $M$. In the end, each row is scaled to prevent bias from differing amounts of submissions.
The mean for an element $e$ can be determined by going through each hash function $h_i\in H$ and computing the position in that row's vector determined by the hash function applied to the element ($M[i,h_i(e)]$), and calculating their mean.

To save bandwidth, the Hadamard count mean sketch algorithm (HCMS) only sends a single bit instead of the vector. To reduce the error, a Hadamard basis transform is applied to the vector. The randomly sampled bit is then anonymized by a random flip with probability $\frac{1}{exp(\epsilon)+1}$. The chosen hash function $h_i$, bit position $j$, and anonymized bit $b$ are sent to the server. The server side is similar to CMS in that it adds the (debiased) bit to a sketch matrix at the position $(i,j)$. The Hadamard basis transform needs to be reversed, before an element's mean can be determined as with CMS.

\subsection{Machine learning}
Machine learning systems can be attacked to extract information about the training data.

To mitigate these attacks, the training data can be randomized. Parametrizing a random distribution such that it resembles the actual training data, new random data can be generated that is safe to use for training.

The PATE method partitions the training data and trains multiple models on each part. The predictions of these models are then combined and the prediction that is given most often is chosen. When the models clearly agree on a favorite prediction, there is little risk of leaking sensitive data. To mitigate the issue where the models are split between two predictions, Laplacian noise is added to achieve ambiguity that protects the data.

\section{Privacy by design}
Privacy by design is a paper proposing that privacy in systems should be
\begin{itemize}
  \item proactive,
  \item private by default,
  \item embedded,
  \item fully functional,
  \item end to end,\sidenote{With respect to lifetime, too.}
  \item transparent, and
  \item user-centric.
\end{itemize}

Privacy engineering is an umbrella term encompassing the domains required for building privacy-respecting systems: Legal (policy), engineering (architecture), and design (interaction).

\clearpage
\printbibliography


\end{document}
